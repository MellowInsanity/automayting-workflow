# Automayting your workflow setup at login!
## Why?
Because I'm lazy, and I started shell scripting recently. So I thought why not
put that knowledge to some use and become more lazy!

## What does this do?
Well, my current workflow is as follows:
### Workspace 1
1. Open up my Udemy course (which btw is an awesome course for any C programmer!)
2. Open up a konsole terminal, with my current source root directory, so that I 
can instantly fire up vim and start coding

### Workspace 2
Open up a firefox window, and load up a random playlist of music (which I have
handpicked)
so it does exactly that...

## Will this work on my machine?
Well, I am running KDE as of now, and the command that switches workspaces is 
specific to KDE. You'll have to look up how to switch up workspaces in your
own Desktop Environment (be it GNOME, XFCE or a WM like i3 or DWM). Change 
the function `change_workspace()` in startup file accordingly, and the rest 
will work fine.

You can also customize the playlist to your own likings! Just paste newline
seperated list of links to the tracks you want to have in your sample space,
and change the `$RAND % n` value in `random_music`, where n is the number of
tracks in `SONG_LIST`

## I want to add more stuff into this script, how do I contribute?
Just clone the repo, make a new branch with whatever name you want, make 
whatever changes you want! And if you feel that the changes you made might
help other people as well, then make a PR as well!
